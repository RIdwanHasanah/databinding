package com.indonesia.ridwan.databiding.Model;

import android.graphics.drawable.Drawable;
import android.widget.Button;

/**
 * Created by hasanah on 9/10/16.
 */
public class Model {

    public Drawable picture;
    public String text1;
    public String text2;
    Button btn;

    public Model(String tex1 , String text2, Drawable picture){

        this.picture = picture;
        this.text1 = tex1;
        this.text2 = text2;
    }
}
