package com.indonesia.ridwan.databiding;

import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.indonesia.ridwan.databiding.Model.Model;
import com.indonesia.ridwan.databiding.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding bd = DataBindingUtil.setContentView(this,R.layout.activity_main);

        Model m = new Model("Bismillah","Tes", ContextCompat.getDrawable(this,R.drawable.x));
        bd.setModel(m);


    }
}
